# Ressources plus complètes

## Encyclopédies

- L'incontournable Wikipedia: [Informatique](https://en.wikipedia.org/wiki/Computer_science)
- Autres wikis: [ComputerScienceWiki](https://computersciencewiki.org/index.php/Welcome)

## MOOCs

Internet regorge de ressources et notamment de cours gratuits.

- Site de cours très divers (français): [OpenClassrooms](https://openclassrooms.com)
- Site de cours très divers (international): [Udemy](https://www.udemy.com/)
- Par Harvard, Boston, IBM...: [edx](https://www.edx.org/)
- Par l'Inria: [FUN-MOOC](https://www.fun-mooc.fr/fr/)
- Vidéos, ressources et formations ludiques: [ClassCode](https://pixees.fr/classcode-v2) ([en](https://pixees.fr/classcode-v2/classcode-informatics-and-digital-creation-online-free-open-course/)) ([Vesrion podcast](https://project.inria.fr/classcode/profiter-de-classcode-en-postcast/)) ([Focus AI](https://pixees.fr/classcode-v2/iai/))
- Pour appréhender l'IA: [Elements of AI](https://www.elementsofai.fr/), [ML Expert](https://www.algoexpert.io/machine-learning/crash-course)
- Cyber-sécurité: [Try Hack Me](https://tryhackme.com/), [Port Swigger](https://portswigger.net/web-security)

## Chaînes YouTube

- Micode: [Underscore](https://www.youtube.com/c/UnderscoreTalk) (live [Twitch](https://www.twitch.tv/micode) le mercredi à 19h), [Bref news](https://www.youtube.com/channel/UCc3auP4KRed7dDBQEtYslbw), [Enquêtes](https://www.youtube.com/c/Micode-Enqu%C3%AAtes)
- Discours inspirants: [TED](https://www.youtube.com/c/TED), [TEDxTalks](https://www.youtube.com/user/TEDxTalks/search?query=Computer%20science)
- Pour des vidéos de python (principalement): [Docstring](https://www.youtube.com/c/Docstring) ([Site](https://www.docstring.fr/)), [Foxypy](https://www.youtube.com/c/foxxpy)
- Pour des vidéos de programmation en français: [Graven](https://www.youtube.com/c/Gravenilvectuto)
- Pour des vidéos de programmation en anglais: [Keep on Coding](https://www.youtube.com/c/KeepOnCoding)
- AI en français: [Machine Learnia](https://www.youtube.com/c/MachineLearnia), [Defend Intelligence](https://www.youtube.com/c/DefendIntelligence-tech) ([Twitch](https://www.twitch.tv/defendintelligence))
- AI en anglais:[The AI Guy](https://www.youtube.com/c/TheAIGuy)
- Informatique théorique: [Informatique Théorique](https://www.youtube.com/channel/UC4m4TCpEwpHSxmwr3zdkGXQ),
- Cyber-sécurité:

## Groupes d'aide Discord

- Programmation: [Docstring](https://discord.com/invite/docstring)
- AI: [MachineLearnia](https://discord.com/invite/WMvHpzu), [DefendIntelligence](https://discord.gg/8nJfrUhGGm)
- Cyber-sécurité: Root-me, TryHackMe, NetworkChuck
