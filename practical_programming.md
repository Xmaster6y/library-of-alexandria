# L'informatique en pratique

## Organiser son espace de travail

- Organiser: [Notion](https://www.notion.so/)
- Communiquer: Discord
- Éditeurs de code: VSCode
- Virtualisation: Virtualbox (Windows, Linux), WSL, Docker

## Pratiquer en s'amusant

### Plateformes de jeux, défis, etc

- Programmer en s'amusant: [Codingame](https://www.codingame.com/start)
- Créer des circuits logiques: [Nand Game](https://nandgame.com/), [Hardware engineering](https://store.steampowered.com/app/525610/Hardware_Engineering/) (sur steam)
- Petit exercices/défis pratiques (type interview) sur de thèmes/langages variés : [Hacker Rank](https://www.hackerrank.com/), [Code Wars](https://www.codewars.com/), [Top Coder](https://www.topcoder.com/), [Leet Code](https://leetcode.com/)
- Exercices/défis sur le thème du Web: [Daily UI](https://www.dailyui.co/), [Front End Mentor](https://www.frontendmentor.io/challenges)
- Cyber-sécurité: [Root Me](https://www.root-me.org/), [Try Hack Me](https://tryhackme.com/)

### Challenges de programmation

- [Isograd](https://www.isograd-testingservices.com/FR/solutions-challenges-de-code?contest_id=49&que_str_id=%C2%AE_typ_id=2)
- CTF (challenge de cyber-sécurité): [CTF Time](https://ctftime.org/) (planning de CTF)
- Cyber-sécurité: [Root Me](https://www.root-me.org/), [DGHack](https://www.dghack.fr/), [Capture the Ether](https://capturetheether.com/) (challenge en lien avec la crypto-monaie), [Ledger Ops](https://ledgerops.com/blog/)(Compte-rendus de challenges)
- Challenge d'IA: [Kaggle](https://www.kaggle.com/competitions), [Challenge Data](https://challengedata.ens.fr/)

## Colaborer sur un projet

- IDE en ligne:[Replit](https://replit.com/)
- Par Google (lien avec Google Drive): [Google Colab](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwilmcecsuv2AhXLh1wKHYX_ClIQFnoECAkQAQ&url=https%3A%2F%2Fresearch.google.com%2Fcolaboratory%2F&usg=AOvVaw38J01zt_Dlb6pQ1fe6FGrI)

## Prise en main de Linux

### Choisir et installer une distribution linux

La distribution la plus en vogue:

- Ubuntu (accessible aux débutants): [Guide d'installation](https://ubuntu.com/server/docs/installation)

Une alternative plus décentralisée et légère:

- ArchLinux (pour les téméraires): [Guide d'installation](https://wiki.archlinux.org/title/Installation_guide), [Wiki](https://archlinux.org/)

Une des meilleurs distributions Linux (basée sur ArchLinux):

- Manjaro (pour les gamers, les geeks, les devs ET les débutants): [Guide d'installation](https://manjaro.org/support/firststeps/), [Wiki](https://wiki.manjaro.org/index.php/Main_Page), [egpu-switcher](https://github.com/hertg/egpu-switcher) (pour les gamers et les devs AI)

### Des outils presque indispensable

- Editeurs de texte: [`vim`](https://doc.ubuntu-fr.org/vim)
- Emulateur de terminal: [`tmux`](https://doc.ubuntu-fr.org/tmux)
- Virtualisation: [`docker`](https://doc.ubuntu-fr.org/docker)

## Programmation Web

- Faire un site à partir de fichiers texte: [mkdocs](https://squidfunk.github.io/mkdocs-material/)
- Tester des requêtes HTTP: [Request bin](https://pipedream.com/requestbin)

## Langages de programmation: Apprendre un langage

- _**Scratch**_ : [Classcode](https://project.inria.fr/classcode/initiation-a-scratch-en-autonomie/)
- _**Python**_ : [Excellent pdf](https://inforef.be/swi/download/apprendre_python3_5.pdf) (un cours très bon et très complet), [Gloassaire](https://docs.python.org/fr/3/glossary.html#glossary)([fr](https://upylab.ulb.ac.be/pub/glossaire_python/Modules/glossaire_extrait.html))
- _**Mardown**_ : [Markdown tutorial]](https://www.markdowntutorial.com/fr/)
- _**C**_ (débutant): [Tutorials Point](https://www.tutorialspoint.com/cprogramming/index.htm)

Plus que du simple apprentissage:

- Pour poser des questions mais surtout lire des reponses: [Stack Overflow](https://stackoverflow.com/)
- Trouver des contrats de freelance (programmation, design,...): [Fiverr](https://fr.fiverr.com/), [5euros](https://5euros.com/) (copie française de Fiverr), [Upwork](https://www.upwork.com/)
- Comunauté freelance python: [AFPY](https://www.afpy.org/emplois)

## Langages de programmation: Librairies incontournables

### Python

Avant tout il faut explorer la [librairie standard](https://docs.python.org/3/library/index.html)!

- Pour s'amuser: [Discord](https://discordpy.readthedocs.io/en/stable/)
- Data-Science: [Pandas](https://pandas.pydata.org/pandas-docs/stable/getting_started/index.html), [Numpy](https://numpy.org/), [Scipy](https://scipy.org/)
- Tracer des données: [Matplotlib](https://matplotlib.org/), [Plotly](https://plotly.com/python/plotly-fundamentals/)
- Récolter des données: [Requests](https://docs.python-requests.org/en/latest/), [Selenium](https://selenium-python.readthedocs.io/), [Scrapy](https://scrapy.org/)
- Machine Learning: [Scikit-learn](https://scikit-learn.org/stable/getting_started.html)
- Deep Learnin:
- Accélérer python: [Numba](https://numba.pydata.org/), [Cython](https://cython.org/)

### C

## Travailler sa curiosité

### Robotique

- Arduino
- [Thymio](https://www.fun-mooc.fr/fr/cours/le-robot-thymio-comme-outil-de-decouverte-des-sciences-du-numerique/)

### Le Darknet

- Demistifier le Darknet: [Publication](https://www.researchgate.net/profile/Vasilis-Ververis-2/publication/326364231_Demistifying_the_dark_web/links/5c0d34834585157ac1b6ad78/Demistifying-the-dark-web.pdf?origin=publication_detail), TED talks ([1](https://www.youtube.com/watch?v=HfuZJVpNWR4),[2](https://www.youtube.com/watch?v=pzN4WGPC4kc),[3](https://www.youtube.com/watch?v=luvthTjC0OI))
- Navigateur: [Installation de Tor](https://tb-manual.torproject.org/installation/) ([fr](https://tb-manual.torproject.org/fr/installation/)), History ([fr](https://www.torproject.org/fr/about/history/)), [Utilisation de Tor avec|sans VPN](https://www.expressvpn.com/blog/tor/)
- Repertoires de sites: [DarkDotFail](https://dark.fail/)

## Apréhender le Cloud

Pour pouvoir programmer même avec un ordinateur des années 2000 ou une tablette.

- [OVH](https://corporate.ovhcloud.com/fr/) [AI](https://www.ovhcloud.com/fr/public-cloud/ai-training/)
- AWS
- Google Colab
