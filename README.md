# Library of Alexandria

Littéralement "la bibliothèque d'Alexandrie", ce dépôt a simplement pour but de
rassemebler un maximum de ressources pour apprendre et pratiquer l'informatique.

---
Ce dépôt est humblement non-exhaustif contrairement à ce que pourrait laisser
penser son nom.

## Disclaimer: Ressources en anglais

Indéniablement l'anglais reste la langue de l'informatique. Ainsi la plupart des
ressources (notamment les documentations de librairies) sont en anglais. Lorsque
cela est possible un équivalent est donné en français.

## Structure du dépôt

La structure se veut scinder en trois principaux thèmes regroupés dans trois
fichier Markdown.

### L'informatique en pratique [->](./practical_programming.md)

Pour apprendre un langage, prendre en main Linux ou mettre en place un environnement de travail adapté.

### La théorie de l'informatique [->](./theoretical_programming.md)

Pour creuser la théorie, l'histoire et le fonctionnement de l'informatique.

### Ressources plus complètes [->](./more_comprehensive_ressources.md)

Pour aborder plus sérieusement la pratique de l'informatique ainsi que pour découvrir et approfondir certains sujet.
